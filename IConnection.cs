﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    public interface IDatabaseConnection
    {
        // get SqlConnection to specific database
        SqlConnection SqlConnection { get; }
    }
}
