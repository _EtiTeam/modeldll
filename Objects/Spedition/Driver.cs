﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    [DataContract]
    public class Driver : IDatabaseObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int? Id { get; set; }

        [DataMember(Name = "Name", Order = 1)]
        public string Name { get; set; }

        [DataMember(Name = "Surname", Order = 2)]
        public string Surname { get; set; }

        [DataMember(Name = "Capacity", Order = 3)]
        public int Capacity { get; set; }


        public string getTableName => TableName;
        static public string TableName => "Drivers";

        public string InsertCommand => $"insert into { getTableName } (Name, Surname, Capacity) values ('{ Name }', '{ Surname }','{ Capacity }')";
        public string UpdateCommand => $"update { getTableName } set Name = '{ Name }', Surname = '{ Surname }', Capacity = '{ Capacity }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";
        static public string SelectCommand =>   $"select * from { TableName }";
        static public string CreateCommand =>   $"CREATE TABLE { TableName } (" +
                                                    "id int PRIMARY KEY IDENTITY," +
                                                    "Name varchar(255) NOT NULL," +
                                                    "Surname varchar(255) NOT NULL," +
                                                    "Capacity int NOT NULL);";
       static public string mockValuesCommand =>    $"INSERT INTO { TableName } VALUES ('Frank', 'Gesek',55);" +
                                                    $"INSERT INTO { TableName } VALUES ('Elivs', 'Krawczyk',33);" +
                                                    $"INSERT INTO { TableName } VALUES ('Neil', 'Cugowski',120);" +
                                                    $"INSERT INTO { TableName } VALUES ('Gerry', 'Kowalski',150);";

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<Driver>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
           => ModelOperations.SelectUsingEstablishConnection<Driver>(SelectThisCommand, cmd).First();
    }
}
