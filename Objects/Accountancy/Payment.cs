﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    [DataContract]
    public class Payment : IDatabaseObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int? Id { get; set; }

        [DataMember(Name = "OrderID", Order = 1)]
        public int OrderID { get; set; }

        [DataMember(Name = "Value", Order = 2)]
        public string Value { get; set; }

        [DataMember(Name = "PaymentState", Order = 3)]
        public string PaymentState { get; set; }

        [DataMember(Name = "PaymentDate", Order = 4)]
        public string PaymentDate { get; set; }
        

        public string getTableName => TableName;
        static public string TableName = "Payments";

        public string InsertCommand => $"insert into { getTableName } (OrderID, Value, PaymentState, PaymentDate) values ('{ OrderID }', '{ Value }', '{ PaymentState }', '{ PaymentDate }')";
        public string UpdateCommand => $"update { getTableName } set OrderID = '{ OrderID }', Value = '{ Value }', PaymentState = '{ PaymentState }', PaymentDate = '{ PaymentDate }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";
        static public string SelectCommand => $"select * from { TableName }";
        static public string CreateCommand => $"CREATE TABLE { TableName } (" +
                                            "id int PRIMARY KEY IDENTITY," +
                                            "OrderID int," +
                                            "Value int NOT NULL," +
                                            "PaymentState varchar(255) NOT NULL," +
                                            "PaymentDate DateTime NOT NULL);";
        static public string mockValuesCommand = $"INSERT INTO { TableName } VALUES (123, 50, 'DONE', '2017-07-10 12:01:49');" +
                                                 $"INSERT INTO { TableName } VALUES (345, 75, 'DONE', '2017-07-13 15:11:23');" +
                                                 $"INSERT INTO { TableName } VALUES (567, 30, 'WAITING', '2017-07-18 10:21:23');";

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<Payment>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
             => ModelOperations.SelectUsingEstablishConnection<Payment>(SelectThisCommand, cmd).First();
    }
}
