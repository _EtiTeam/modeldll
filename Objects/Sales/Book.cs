﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    [DataContract]
    public class Book : IDatabaseObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int? Id { get; set; }

        [DataMember(Name = "Title", Order = 1)]
        public string Title { get; set; }

        [DataMember(Name = "Author", Order = 2)]
        public string Author { get; set; }

        [DataMember(Name = "ReleaseYear", Order = 3)]
        public int ReleaseYear { get; set; }

        [DataMember(Name = "ISBN", Order = 4)]
        public int ISBN { get; set; }

        [DataMember(Name = "Price", Order = 5)]
        public int Price { get; set; }

        [DataMember(Name = "Amount", Order = 6)]
        public int Amount { get; set; }

        public string getTableName => TableName;
        static public string TableName => "Books";

        public string InsertCommand => $"insert into { getTableName } (Title, Author, ReleaseYear, ISBN, Price, Amount) values ('{ Title }', '{ Author }', '{ ReleaseYear }', '{ ISBN }', '{ Price }', '{ Amount }')";
        public string UpdateCommand => $"update { getTableName } set Title = '{ Title }', Author = '{ Author }', ReleaseYear = '{ ReleaseYear }', ISBN = '{ ISBN }', Price = '{ Price }', Amount = '{ Amount }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";
        static public string SelectCommand => $"select * from { TableName }";
        static public string CreateCommand => $"CREATE TABLE { TableName} ("+
                                            "ID int PRIMARY KEY IDENTITY," +
                                            "Title varchar(255) NOT NULL," +
                                            "Author varchar(255) NOT NULL," +
                                            "ReleaseYear decimal(4,0) NOT NULL," +
                                            "ISBN int NOT NULL," +
                                            "Price int NOT NULL," +
                                            "Amount int NOT NULL)";
       static public string mockValuesCommand => $"INSERT INTO { TableName } VALUES ('The Bible', 'Unknown', 1960, 132456789, 50, 13);" +
                                                 $"INSERT INTO { TableName } VALUES ('Fairytales', 'Hans Andersen', 1890, 456123789, 75, 5);" +
                                                 $"INSERT INTO { TableName } VALUES ('HorrorBook', 'Edgar Alan Poe', 1990, 789654132, 40, 8);";

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<Book>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
            => ModelOperations.SelectUsingEstablishConnection<Book>(SelectThisCommand, cmd).First();
    }
}
