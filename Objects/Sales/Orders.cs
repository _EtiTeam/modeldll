﻿using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;

namespace ModelDLL
{
    [DataContract]
    public class Order : IDatabaseObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int? Id { get; set; }

        [DataMember(Name = "CustomerID", Order = 1)]
        public int CustomerID { get; set; }

        [DataMember(Name = "BookID", Order = 2)]
        public int BookID { get; set; }

        [DataMember(Name = "OrderState", Order = 3)]
        public string OrderState { get; set; }

        [DataMember(Name = "OrderDate", Order = 4)]
        public string OrderDate { get; set; }

        public string getTableName => TableName;
        static public string TableName => "orders";

        public string UpdateStateCommand => $"update { getTableName } set OrderState = '{ OrderState }' where id = '{ Id }'";
        public string InsertCommand => $"insert into { getTableName } (CustomerID, BookID, OrderState, OrderDate) values ('{ CustomerID }', '{ BookID }', '{ OrderState }', '{ OrderDate }')";
        public string UpdateCommand => $"update { getTableName } set CustomerID = '{ CustomerID }', BookID = '{ BookID }', OrderState = '{ OrderState }', OrderDate = '{ OrderDate }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";
        static public string SelectCommand => $"select * from { TableName }";
        static public string CreateCommand => $"CREATE TABLE { TableName } (" +
                                            "ID int PRIMARY KEY IDENTITY," +
                                            "CustomerID int REFERENCES Customers," +
                                            "BookID int REFERENCES Books," +
                                            "OrderState varchar(255) NOT NULL," +
                                            "OrderDate DateTime NOT NULL" +
                                        ")";
        static public string mockValuesCommand =>   $"INSERT INTO { TableName } VALUES (2,1,'SAMPLE_STATE','2017-07-08 12:12:12');" +
                                                    $"INSERT INTO { TableName } VALUES (1,1,'SAMPLE_STATE','2017-07-05 15:14:42');" +
                                                    $"INSERT INTO { TableName } VALUES (3,1,'SAMPLE_STATE','2017-07-06 11:13:10');";

        public const string STATE_WAITING_FOR_PAYMENT = "Waiting for payment";
        public const string STATE_WAITING_FOR_DELIVERY = "Waiting for delivery";
        public const string STATE_COMPLETED = "Completed";

        public void UpdateState(IDatabaseConnection databaseConnection) 
            => ModelOperations.ExecuteSqlCommand(UpdateStateCommand, databaseConnection);

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<Order>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
          => ModelOperations.SelectUsingEstablishConnection<Order>(SelectThisCommand, cmd).First();
    }
}
