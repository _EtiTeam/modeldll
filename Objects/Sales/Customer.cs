﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    [DataContract]
    public class Customer : IDatabaseObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int? Id { get; set; }

        [DataMember(Name = "Name", Order = 1)]
        public string Name { get; set; }

        [DataMember(Name = "Surname", Order = 2)]
        public string Surname { get; set; }

        public string getTableName => TableName;
        static public string TableName => "customers";

        public string InsertCommand => $"insert into { getTableName } (Name, Surname) values ('{ Name }', '{ Surname }')";
        public string UpdateCommand => $"update { getTableName } set Name = '{ Name }', Surname = '{ Surname }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";
        static public string SelectCommand => $"select * from { TableName }";
        static public string CreateCommand => $"CREATE TABLE { TableName } (" +
                                            "ID int PRIMARY KEY IDENTITY," +
                                            "Name varchar(255) NOT NULL," +
                                            "Surname varchar(255) NOT NULL" +
                                        ")";
        static public string mockValuesCommand =>   $"INSERT INTO { TableName } VALUES ('Frank', 'Sinatra');" +
                                                    $"INSERT INTO { TableName } VALUES ('Elivs', 'Presley');" +
                                                    $"INSERT INTO { TableName } VALUES ('Neil', 'Armstrong');" +
                                                    $"INSERT INTO { TableName } VALUES ('Gerry', 'Raffrerty');";

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<Customer>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
           => ModelOperations.SelectUsingEstablishConnection<Customer>(SelectThisCommand, cmd).First();
    }
}
