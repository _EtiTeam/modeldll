﻿using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;

namespace ModelDLL
{
    [DataContract]
    public class User : IDatabaseObject
    {
        [DataMember(Name = "id")]
        public int? Id { get; set; }

        [DataMember(Name = "login")]
        public string Login { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "sessionUUID")]
        public string SessionUUID { get; set; }

        public string getTableName => TableName;
        static public string TableName => "users";

        public string InsertCommand => $"insert into { getTableName } (login, password, sessionUUID) values ('{ Login }', '{ Password }', '{ SessionUUID }')";
        public string UpdateCommand => $"update { getTableName } set login = '{ Login }', password = '{ Password }', sessionUUID = '{ SessionUUID }' where id = '{ Id }'";
        public string DeleteCommand => $"delete from { getTableName } where id = '{ Id }'";

        static public string SelectCommand => $"select * from { TableName }";
        static public string CreateCommand => $"CREATE TABLE { TableName } ( " +
                    "id int not null identity," +
                    "login nvarchar(50) not null ," +
                    "password nvarchar(128) not null," +
                    "sessionUUID nvarchar(36)," +
                    "primary key(id)" +       
            ")";

        public string SelectThisCommand => SelectCommand + $" where id = '{ Id }'";

        public IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection) 
            => ModelOperations.Select<User>(SelectThisCommand, iDatabaseConnection).FirstOrDefault();

        public IDatabaseObject FetchUsingEstablishConnection(SqlCommand cmd)
            => ModelOperations.SelectUsingEstablishConnection<User>(SelectThisCommand, cmd).First();
    }
}
