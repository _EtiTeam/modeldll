﻿using System.Data.SqlClient;

namespace ModelDLL
{
    public interface IDatabaseObject
    {
        string getTableName { get; }

        int? Id { get; set; }

        string InsertCommand { get; }
        string UpdateCommand { get; }
        string SelectThisCommand { get; }
        string DeleteCommand { get; }
        // fetch the a object with the same id. This object will have actual data from databases
        IDatabaseObject Fetch(IDatabaseConnection iDatabaseConnection);
        IDatabaseObject FetchUsingEstablishConnection(SqlCommand c);

        //string toJson(); // TODO
    }
}
