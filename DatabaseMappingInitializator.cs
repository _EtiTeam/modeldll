﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDLL
{
    // mapping used by ModelOperations methods
    class DatabaseMappingInitializator
    {
        static DatabaseMappingInitializator() {
            AutoMapper.Mapper.Initialize(cfg => {
                //sales
                cfg.CreateMap<DbDataReader, User>()
                    .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                    .ForMember(m => m.Login, opt => opt.MapFrom(r => r[r.GetOrdinal("login")]))
                    .ForMember(m => m.Password, opt => opt.MapFrom(r => r[r.GetOrdinal("password")]))
                    .ForMember(m => m.SessionUUID, opt => opt.MapFrom(r => r[r.GetOrdinal("sessionUUID")]));
                cfg.CreateMap<DbDataReader, Book>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.Title, opt => opt.MapFrom(r => r[r.GetOrdinal("Title")]))
                   .ForMember(m => m.Author, opt => opt.MapFrom(r => r[r.GetOrdinal("Author")]))
                   .ForMember(m => m.ReleaseYear, opt => opt.MapFrom(r => r[r.GetOrdinal("ReleaseYear")]))
                   .ForMember(m => m.ISBN, opt => opt.MapFrom(r => r[r.GetOrdinal("ISBN")]))
                   .ForMember(m => m.Price, opt => opt.MapFrom(r => r[r.GetOrdinal("Price")]))
                   .ForMember(m => m.Amount, opt => opt.MapFrom(r => r[r.GetOrdinal("Amount")]));
                cfg.CreateMap<DbDataReader, Order>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.CustomerID, opt => opt.MapFrom(r => r[r.GetOrdinal("CustomerID")]))
                   .ForMember(m => m.BookID, opt => opt.MapFrom(r => r[r.GetOrdinal("BookID")]))
                   .ForMember(m => m.OrderState, opt => opt.MapFrom(r => r[r.GetOrdinal("OrderState")]))
                   .ForMember(m => m.OrderDate, opt => opt.MapFrom(r => r[r.GetOrdinal("OrderDate")]));
                cfg.CreateMap<DbDataReader, Customer>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.Name, opt => opt.MapFrom(r => r[r.GetOrdinal("Name")]))
                   .ForMember(m => m.Surname, opt => opt.MapFrom(r => r[r.GetOrdinal("Surname")]));                               
                cfg.CreateMap<DbDataReader, Driver>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.Name, opt => opt.MapFrom(r => r[r.GetOrdinal("Name")]))
                   .ForMember(m => m.Surname, opt => opt.MapFrom(r => r[r.GetOrdinal("Surname")]))
                   .ForMember(m => m.Capacity, opt => opt.MapFrom(r => r[r.GetOrdinal("Capacity")]));
               cfg.CreateMap<DbDataReader, Payment>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.OrderID, opt => opt.MapFrom(r => r[r.GetOrdinal("OrderID")]))
                   .ForMember(m => m.Value, opt => opt.MapFrom(r => r[r.GetOrdinal("Value")]))
                   .ForMember(m => m.PaymentState, opt => opt.MapFrom(r => r[r.GetOrdinal("PaymentState")]))
                   .ForMember(m => m.PaymentDate, opt => opt.MapFrom(r => r[r.GetOrdinal("PaymentDate")]));
                cfg.CreateMap<DbDataReader, WarehouseBook>()
                   .ForMember(m => m.Id, opt => opt.MapFrom(r => r[r.GetOrdinal("id")]))
                   .ForMember(m => m.Title, opt => opt.MapFrom(r => r[r.GetOrdinal("Title")]))
                   .ForMember(m => m.Author, opt => opt.MapFrom(r => r[r.GetOrdinal("Author")]))
                   .ForMember(m => m.ReleaseYear, opt => opt.MapFrom(r => r[r.GetOrdinal("ReleaseYear")]))
                   .ForMember(m => m.ISBN, opt => opt.MapFrom(r => r[r.GetOrdinal("ISBN")]))
                   .ForMember(m => m.Price, opt => opt.MapFrom(r => r[r.GetOrdinal("Price")]))
                   .ForMember(m => m.Amount, opt => opt.MapFrom(r => r[r.GetOrdinal("Amount")]));

                //
            });
        }

        public static void initialize() { } // call the constructor
    }
}
