﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ModelDLL
{
    public static class ModelOperations
    {
        static ModelOperations() {
            DatabaseMappingInitializator.initialize();
        }

        public static void ExecuteSqlCommand(string sqlCommandText, IDatabaseConnection iDatabaseConnection)
        {
            var c = iDatabaseConnection.SqlConnection;
            var cmd = c.CreateCommand();
            using (c) {
                c.Open();
                ExecuteSqlCommandUsingEstablishConnection(sqlCommandText, cmd);
            }
        }

        public static void ExecuteSqlCommandUsingEstablishConnection(string sqlCommandText, SqlCommand cmd)
        {
            cmd.CommandText = sqlCommandText;
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] >> ERROR SQL: {ex.Message}");
                throw ex;
            }
        }

        public static T ExecuteScalar<T>(string sqlCommandText, IDatabaseConnection iDatabaseConnection)
            where T : class
        {
            var c = iDatabaseConnection.SqlConnection;
            var cmd = c.CreateCommand();
            cmd.CommandText = sqlCommandText;
            using (c)
            {
                try
                {
                    c.Open();
                    return (T)cmd.ExecuteScalar();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] >> ERROR SQL: {ex.Message}");
                    throw;
                }
            }
        }

        public static void Insert(IDatabaseObject databaseObject, IDatabaseConnection c)
        {
            try
            {
                var command = databaseObject.InsertCommand + selectLastInsertedIdCommand();
                var id = (int)ExecuteScalar<object>(command, c);
                databaseObject.Id = id;
            }
            catch (SqlException ex)
            {               
                throw ex;
            }
        }

        public static void Insert(IList<IDatabaseObject> list, IDatabaseConnection c) =>
            list.ToList().ForEach(it => Insert(it, c));

        public static void Update(IDatabaseObject databaseObject, IDatabaseConnection c) =>
            ExecuteSqlCommand(databaseObject.UpdateCommand, c);

        public static void UpdateUsingEstablishConnection(IDatabaseObject databaseObject, SqlCommand cmd) =>
            ExecuteSqlCommandUsingEstablishConnection(databaseObject.UpdateCommand, cmd);

        public static void Update(IList<IDatabaseObject> list, IDatabaseConnection c) =>
            list.ToList().ForEach(it => Update(it, c));

        public static void Delete(IDatabaseObject databaseObject, IDatabaseConnection c) =>
            ExecuteSqlCommand(databaseObject.DeleteCommand, c);

        public static void Delete(IList<IDatabaseObject> list, IDatabaseConnection c) =>
            list.ToList().ForEach(it => Delete(it, c));

        public static IList<T> Select<T>(string sqlCommandText, IDatabaseConnection iDatabaseConnection)
                where T : IDatabaseObject {
            var c = iDatabaseConnection.SqlConnection;
            var cmd = c.CreateCommand();
            using (c)
            {
                c.Open();
                return SelectUsingEstablishConnection<T>(sqlCommandText, cmd);
            }
        }

        public static IList<T> SelectUsingEstablishConnection<T>(string sqlCommandText, SqlCommand cmd)
        {
            List<T> databaseObjects = new List<T>();
            try
            {
                cmd.CommandText = sqlCommandText;

                var reader = cmd.ExecuteReader();
                using (reader)
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var databaseObject = AutoMapper.Mapper.Map<DbDataReader, T>(reader);
                            databaseObjects.Add(databaseObject);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] >> ERROR SQL: {ex.Message}");
                throw ex;
            }
            return databaseObjects;
        }

        public static T Select<T>(int id, IDatabaseConnection iDatabaseConnection)
            where T : class, IDatabaseObject, new()
        {
            T obj = new T { Id = id };
            return obj.Fetch(iDatabaseConnection) as T;
        }

        public static T SelectUsingEstablishConnection<T>(int id, SqlCommand cmd)
            where T : class, IDatabaseObject, new()
        {
            T obj = new T { Id = id };
            return obj.FetchUsingEstablishConnection(cmd) as T;
        }


        public static IList<T> GetFromJson<T>(System.IO.Stream s)
                where T : class, IDatabaseObject {
            IList<T> deserializedObject = null;
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(IList<T>));

            using (s) {
                deserializedObject = ser.ReadObject(s) as IList<T>;
            }
            return deserializedObject;
        }

        public static IList<T> GetFromJson<T>(string json)
                where T : class, IDatabaseObject
            => GetFromJson<T>(new MemoryStream(Encoding.UTF8.GetBytes(json ?? "")));

        public static string toJson<T>(IList<T> obj)
        {
            MemoryStream stream = new MemoryStream();
            StreamReader reader = new StreamReader(stream);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(IList<T>));
            using (stream)
            {
                ser.WriteObject(stream, obj);
                stream.Seek(0, SeekOrigin.Begin);
                return reader.ReadToEnd();
            }
        }

        public static string toJson<T>(T obj)
            => toJson<T>(new List<T> { obj });

        public static void DropTableIfExists(string tableName, IDatabaseConnection c)
            => ExecuteSqlCommand($"DROP TABLE IF EXISTS { tableName }", c);

        public static void RecreateTable(string tableName, string createCommand, IDatabaseConnection c) {
            DropTableIfExists(tableName, c);
            ExecuteSqlCommand(createCommand, c);
        }

        // IDictionary<string, SqlParameter> it maps columName->parameter
        [System.Obsolete]
        public static void Insert(string tableName, IDictionary<string, SqlParameter> parameters, IDatabaseConnection sqlConnection) {
            if (parameters == null || parameters.Count <= 0)
                throw new ArgumentException();

            var c = sqlConnection.SqlConnection;
            var cmd = c.CreateCommand();

            StringBuilder querySB = new StringBuilder();
            StringBuilder columnSB = new StringBuilder();
            StringBuilder paramSB = new StringBuilder();

            foreach (var parameter in parameters)
            {
                columnSB.Append($"@parameter.Key,");        // list of columns
                paramSB.Append($"@{ parameter.Key },");     // cmd for binding
                cmd.Parameters.Add(parameter.Value);        // binding
            }
            columnSB.Remove(columnSB.Length - 1, 1);    // remove last ,
            paramSB.Remove(paramSB.Length - 1, 1);      // remove last ,

            querySB.Append($"INSERT INTO { tableName } ");
            querySB.Append("(")
                .Append(columnSB.ToString())
                .Append(")");
            querySB.Append($" VALUES ").
                Append("(").
                Append(paramSB.ToString()).
                Append(")");

            cmd.CommandText = querySB.ToString();

            using (c)
            {
                c.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static bool CheckIfTableExists(string tableName, IDatabaseConnection iDatabaseConnection)
        {
           var intResult = (int)ExecuteScalar<object>(
                $"if not exists (select * from sysobjects where name='{ tableName }' and xtype='U') select 0 else select 1", 
                iDatabaseConnection
            );
            return intResult == 1 ? true : false;
        }

        private static string selectLastInsertedIdCommand() =>
            "SELECT CAST(scope_identity() AS int);";
    }
}
